interface symbolDict {
  [key: string]: string;
}

const PRE_CONVERSION_MAP: symbolDict = {
  " +": " ",
  yy: "y",
  vv: "v",
  "„„": "„",
  "­­": "­",
  "y&": "y",
  "„&": "„",
  "‡u": "u‡",
  wu: "uw",
  " ,": ",",
  " \\|": "\\|",
  "\\\\ ": "",
  " \\\\": "",
  "\\\\": "",
  "\n +": "\n",
  " +\n": "\n",
  "\n\n\n\n\n": "\n\n",
  "\n\n\n\n": "\n\n",
  "\n\n\n": "\n\n"
};

const CONVERSION_MAP: symbolDict = {
  "°": "ক্ক",
  "±": "ক্ট",
  "²": "ক্ষ্ণ",
  "³": "ক্ত",
  "´": "ক্ম",
  µ: "ক্র",
  "¶": "ক্ষ",
  "·": "ক্স",
  "¸": "গু",
  "¹": "জ্ঞ",
  º: "গ্দ",
  "»": "গ্ধ",
  "¼": "ঙ্ক",
  "½": "ঙ্গ",
  "¾": "জ্জ",
  "¿": "্ত্র",
  À: "জ্ঝ",
  Á: "জ্ঞ",
  Â: "ঞ্চ",
  Ã: "ঞ্ছ",
  Ä: "ঞ্জ",
  Å: "ঞ্ঝ",
  Æ: "ট্ট",
  Ç: "ড্ড",
  È: "ণ্ট",
  É: "ণ্ঠ",
  Ê: "ণ্ড",
  Ë: "ত্ত",
  Ì: "ত্থ",
  Î: "ত্র",
  Ï: "দ্দ",
  Ð: "ণ্ড",
  Ñ: "-",
  Ò: '"',
  Ó: '"',
  Ô: "'",
  Õ: "'",
  "×": "দ্ধ",
  Ø: "দ্ব",
  Ù: "দ্ম",
  Ú: "ন্ঠ",
  Û: "ন্ড",
  Ü: "ন্ধ",
  Ý: "ন্স",
  Þ: "প্ট",
  ß: "প্ত",
  à: "প্প",
  á: "প্স",
  â: "ব্জ",
  ã: "ব্দ",
  ä: "ব্ধ",
  å: "ভ্র",
  ç: "ম্ফ",
  é: "ল্ক",
  ê: "ল্গ",
  ë: "ল্ট",
  ì: "ল্ড",
  í: "ল্প",
  î: "ল্ফ",
  ï: "শু",
  ð: "শ্চ",
  ñ: "শ্ছ",
  ò: "ষ্ণ",
  ó: "ষ্ট",
  ô: "ষ্ঠ",
  õ: "ষ্ফ",
  ö: "স্খ",
  "÷": "স্ট",
  ø: "স্ন",
  ù: "স্ফ",
  û: "হু",
  ü: "হৃ",
  ý: "হ্ন",
  ÿ: "ক্ষ",
  þ: "হ্ম",
  A: "অ",
  B: "ই",
  C: "ঈ",
  D: "উ",
  E: "ঊ",
  F: "ঋ",
  G: "এ",
  H: "ঐ",
  I: "ও",
  J: "ঔ",
  K: "ক",
  L: "খ",
  M: "গ",
  N: "ঘ",
  O: "ঙ",
  P: "চ",
  Q: "ছ",
  R: "জ",
  S: "ঝ",
  T: "ঞ",
  U: "ট",
  V: "ঠ",
  W: "ড",
  X: "ঢ",
  Y: "ণ",
  Z: "ত",
  _: "থ",
  "`": "দ",
  a: "ধ",
  b: "ন",
  c: "প",
  d: "ফ",
  e: "ব",
  f: "ভ",
  g: "ম",
  h: "য",
  i: "র",
  j: "ল",
  k: "শ",
  l: "ষ",
  m: "স",
  n: "হ",
  o: "ড়",
  p: "ঢ়",
  q: "য়",
  r: "ৎ",
  s: "ং",
  t: "ঃ",
  u: "ঁ",
  "0": "০",
  "1": "১",
  "2": "২",
  "3": "৩",
  "4": "৪",
  "5": "৫",
  "6": "৬",
  "7": "৭",
  "8": "৮",
  "9": "৯",
  "•": "ঙ্",
  "|": "।"
};

const PRE_SYMBOLS_MAP: symbolDict = {
  "®": "ষ্",
  "¯": "স্",
  "”": "চ্",
  "˜": "দ্",
  "™": "দ্",
  š: "ন্",
  "›": "ন্",
  "¤": "ম্"
};

const REFF: symbolDict = {
  "©": "র্"
};

const POST_SYMBOLS_MAP: symbolDict = {
  "&": "্‌",
  ú: "্প",
  è: "্ন",
  "^": "্ব",
  "‘": "্তু",
  "’": "্থ",
  "‹": "্ক",
  Œ: "্ক্র",
  "—": "্ত",
  Í: "্ত",
  œ: "্ন",
  Ÿ: "্ব",
  "¡": "্ব",
  "¢": "্ভ",
  "£": "্ভ্র",
  "¥": "্ম",
  "¦": "্ব",
  "§": "্ম",
  "¨": "্য",
  ª: "্র",
  "«": "্র",
  "¬": "্ল",
  "­": "্ল",
  Ö: "্র"
};

const KAARS: symbolDict = {
  v: "া",
  w: "ি",
  x: "ী",
  y: "ু",
  z: "ু",
  æ: "ু",
  "“": "ু",
  "–": "ু",
  "~": "ূ",
  ƒ: "ূ",
  "‚": "ূ",
  "„": "ৃ",
  "…": "ৃ",
  "†": "ে",
  "‡": "ে",
  ˆ: "ৈ",
  "‰": "ৈ",
  Š: "ৗ"
};

const KAAR_POST_CONVERSION: symbolDict = {
  "ো": "ো",
  "ৌ": "ৌ"
};

const POST_CONVERSION_MAP: symbolDict = {
  অা: "আ",
  "্‌্‌": "্‌"
};

const ALL_SYMBOLS: symbolDict = Object.assign(
  {},
  CONVERSION_MAP,
  PRE_SYMBOLS_MAP,
  POST_SYMBOLS_MAP
);

function escapeRegExp(string: string): string {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function createConversionPattern(symbols: symbolDict, delimiter = ""): string {
  return Object.keys(symbols)
    .map(
      (val: string): string => {
        return escapeRegExp(val);
      }
    )
    .join(delimiter);
}

const SYMBOLS_CONVERSION_PATTERN: RegExp = new RegExp(
  `([${createConversionPattern(ALL_SYMBOLS, "")}])`,
  "g"
);

const MAIN_CONVERSION_PATTERN: RegExp = new RegExp(
  `([w†‡ˆ‰Š]?)(([${createConversionPattern(
    PRE_SYMBOLS_MAP
  )}])*([${createConversionPattern(
    CONVERSION_MAP,
    ""
  )}])?([${createConversionPattern(
    POST_SYMBOLS_MAP
  )}])*)([${createConversionPattern(
    REFF
  )}])?([ævxyz“–~ƒ‚„…]?)([${createConversionPattern(POST_SYMBOLS_MAP)}])*`,
  "g"
);

const HASAANT_PATTERN: RegExp = new RegExp(`(${escapeRegExp("্")})+`);

const PRE_CONVERSION_PATTERN: RegExp = new RegExp(
  `(${createConversionPattern(PRE_CONVERSION_MAP, "|")})`,
  "g"
);

const POST_CONVERSION_PATTERN: RegExp = new RegExp(
  `(${createConversionPattern(POST_CONVERSION_MAP, "|")})`,
  "g"
);

function replaceSymbol(m: string): string {
  return (ALL_SYMBOLS as any)[m] || "";
}

function mainConverter(
  match: string,
  preKaar: string,
  mUnit: string,
  g3: string,
  g4: string,
  g5: string,
  reff: string,
  postKaar: string,
  postPhala: string,
  offset: number,
  string: string
): string {
  let core = mUnit.replace(SYMBOLS_CONVERSION_PATTERN, replaceSymbol);
  core = core.replace(HASAANT_PATTERN, m => "্");
  core = reff ? "র্" + core : core;
  core = postPhala ? core + POST_SYMBOLS_MAP[postPhala] : core;
  let kaarString = `${preKaar ? (KAARS as any)[preKaar] : ""}${
    postKaar ? (KAARS as any)[postKaar] : ""
  }`;
  core = core + ((KAAR_POST_CONVERSION as any)[kaarString] || kaarString);
  return core;
}

export default function bnBijoy2Unicode(string: string): string {
  let convText = string.replace(
    PRE_CONVERSION_PATTERN,
    (m: string) => (PRE_CONVERSION_MAP as any)[m] || string
  );
  convText = convText.replace(MAIN_CONVERSION_PATTERN, mainConverter);
  convText = convText.replace(
    POST_CONVERSION_PATTERN,
    (m: string) => (POST_CONVERSION_MAP as any)[m] || convText
  );
  return convText;
}
