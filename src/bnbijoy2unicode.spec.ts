import btou from "./index";

describe("convertion", () => {
  it("Try a simple bijoy sentence to unicode", () => {
    expect(btou("Avgvi †mvbvi evsjv, Avwg †Zvgvq fv‡jvevwm|")).toEqual(
      "আমার সোনার বাংলা, আমি তোমায় ভালোবাসি।"
    );
    expect(btou("fvlvšÍi")).toEqual("ভাষান্তর");
    expect(btou("g„Zz¨")).toEqual("মৃত্যু");
    expect(btou("ÿwZ")).toEqual("ক্ষতি");
  });
});
