# bnbijoy2unicode

### Install

```
yarn add @codesigntheory/bnbijoy2unicode
```

### Usage

Import and use. bnUnicode2ANSI is the function you need.

```javascript
bnBijoy2Unicode("Avgvi †mvbvi evsjv, Avwg †Zvgvq fv‡jvevwm|");
```

### Development

1. First, install dependencies with `yarn`
2. Then, Clone -> Code -> Send pull request.

### Issues?

Please open ticket in github.
